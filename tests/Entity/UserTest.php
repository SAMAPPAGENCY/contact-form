<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\Authentication\User;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        $this->user = new User();
    }


    public function testIfEntityHaveIdProperty(): void
    {
        $id = 1;
        $this->user->setId($id);

        $this->assertEquals($id, $this->user->getId());
    }

    public function testIfEntityHaveUsernameProperty(): void
    {
        $username = "webmaster@live.fr";
        $this->user->setUsername($username);

        $this->assertEquals($username, $this->user->getUsername());
    }

    public function testIfEntityHavePasswordProperty(): void
    {
        $password = "password";
        $this->user->setPassword($password);

        $this->assertEquals($password, $this->user->getPassword());
    }
}
