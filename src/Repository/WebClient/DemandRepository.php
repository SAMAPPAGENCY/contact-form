<?php

namespace App\Repository\WebClient;

use App\Entity\WebClient\Demand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Demand|null find($id, $lockMode = null, $lockVersion = null)
 * @method Demand|null findOneBy(array $criteria, array $orderBy = null)
 * @method Demand[]    findAll()
 * @method Demand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Demand::class);
    }

    public function getAllCheckedDemands()
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.isCheck = :val')
            ->setParameter('val', true)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getUncheckedDemands()
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.isCheck = :val')
            ->setParameter('val', false)
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Demand
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
