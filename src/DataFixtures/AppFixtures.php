<?php

namespace App\DataFixtures;

use App\Entity\Authentication\User;
use App\Entity\Authentication\UserRole;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher= $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $webmasterRole = new UserRole;
        $webmasterRole->setLibelle("ROLE_WEB_MASTER");

        $webmaster = new User;
        $webmaster->setUsername("webmaster@live.fr")
                  ->setPassword($this->hasher->hashPassword($webmaster, "webmaster"))
                  ->addUserRole($webmasterRole);

        $manager->persist($webmasterRole);
        $manager->persist($webmaster);

        $manager->flush();
    }
}
