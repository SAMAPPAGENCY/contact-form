<?php

namespace App\Form\Contact;

use App\Entity\WebClient\Demand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\Email;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mail', EmailType::class, [
                'attr' => [
                    "class"       => "item",
                    "placeholder" => "Renseigner votre email"
                ],'constraints' => [
                    new NotBlank(),
                    new Email(
                        [],
                        'Veuillez renseigner un email valide.'
                    ),
                ],      
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    "class" => "item",
                    "placeholder" => "Renseigner votre nom d'usage"
                ], 'constraints' => [
                    new NotBlank(),
                    new Length(
                        [
                            'min' => 3,
                            'minMessage' => 'Veuillez renseigner au moins 3 caractères minimum.'
                        ],
                    ),
                ],
            ])
            ->add('firstname', TextType::class, [
                'attr' => [
                    "class" => "item",
                    "placeholder" => "Renseigner votre prénom"
                ], 'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Veuillez renseigner au moins 3 caractères minimum.'
                    ]),
                    
                ],
            ])
            ->add('message', TextareaType::class, [
                'attr' => [
                    "class" => "item",
                    "placeholder" => "Exprimer votre demande"
                ], 'constraints' => [
                    new NotBlank(),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // 'data_class' => Demand::class,
        ]);
    }
}
