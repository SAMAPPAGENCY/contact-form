<?php
namespace App\Services\file;

use Symfony\Component\HttpKernel\KernelInterface;
use App\Services\file\interface\DownloadFileServiceInterface;

/**
 * DownloadFileService
 * 
 * @property KernelInterface $kernel
 * 
 * @method string|false readFile()
 * 
 * @author Samir Founou <samir_615@live.fr>
 */
class DownloadFileService implements DownloadFileServiceInterface
{
    private KernelInterface $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * Allow to read file from private folder
     *
     * @param string $privateFolder
     * @param string $filename
     * 
     * @return string|false
     * @author Samir Founou <samir_615@live.fr>
     */
    public function readFile(string $privateFolder, string $filename) : string|false
    {
        return file_get_contents($this->kernel->getProjectDir() . $privateFolder . $filename);
    }
}