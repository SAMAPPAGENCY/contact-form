<?php
namespace App\Services\file\interface;

interface DownloadFileServiceInterface
{
    /**
     * Allow to read file from private folder
     *
     * @param string $privateFolder
     * @param string $filename
     * 
     * @return string|false
     * @author Samir Founou <samir_615@live.fr>
     */
    public function readFile(string $privateFolder, string $filename) : string|false;
}