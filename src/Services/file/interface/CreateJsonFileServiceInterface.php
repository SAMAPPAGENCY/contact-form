<?php
namespace App\Services\file\interface;

interface CreateJsonFileServiceInterface
{
    /**
     * Allow to add json file into private folder
     *
     * @return void
     * @author Samir Founou <samir_615@live.fr>
     */
    public function addJsonFileIntoFolder() : void;

    /**
     * Allow to crate json file infos
     *
     * @return array
     * @author Samir Founou <samir_615@live.fr>
     */
    public function createJsonFileInfos() : array;

    /**
     * Get the value of demand
     */ 
    public function getDemand();

    /**
     * Set the value of demand
     *
     * @return  self
     */ 
    public function setDemand($demand);
}