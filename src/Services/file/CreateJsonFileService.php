<?php
namespace App\Services\file;

use App\Entity\WebClient\Demand;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Services\file\interface\CreateJsonFileServiceInterface;

/**
 * CreateJsonFileService
 * 
 * @property const PRIVATE_DEMAND_FOLDER
 * @property KernelInterface $kernel
 * @property Demand $demand
 * 
 * @method void addJsonFileIntoFolder()
 * @method array createJsonFileInfos()
 * @method Demand getDemand()
 * @method self setDemand(Demand $demand)
 * 
 * @author Samir Founou <samir_615@live.fr>
 */
class CreateJsonFileService implements CreateJsonFileServiceInterface
{
    private const PRIVATE_DEMAND_FOLDER = '/private/demand/';
    private KernelInterface $kernel;
    private Demand $demand;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * Allow to add json file into private folder
     *
     * @return void
     * @author Samir Founou <samir_615@live.fr>
     */
    public function addJsonFileIntoFolder() : void
    {
        try {
            file_put_contents(
                $this->kernel->getProjectDir() . self::PRIVATE_DEMAND_FOLDER . $this->getDemand()->getFilename(), 
                json_encode($this->createJsonFileInfos())
            );
        } catch (\Exception $e) {
            dd($e->getMessage());
        }   
    }

    /**
     * Allow to crate json file infos
     *
     * @return array
     * @author Samir Founou <samir_615@live.fr>
     */
    public function createJsonFileInfos() : array
    {   
        /**
         * @var Prospect
         */
        $prospect = $this->getDemand()->getProspect();

        $jsonFileInfos = [
            "prospectInfos" => [
                "lastname"    => $prospect->getLastname(),
                "firstname"   => $prospect->getFirstname(),
                "mail"        => $prospect->getMail()
            ],
            "demand"        => [
                "date_demand" => $this->getDemand()->getCreatedAt()->format('d/m/Y'),
                "message"     => $this->getDemand()->getMessage()
            ]
        ];

        return $jsonFileInfos;
    }

    /**
     * Get the value of demand
     */ 
    public function getDemand()
    {
        return $this->demand;
    }

    /**
     * Set the value of demand
     *
     * @return  self
     */ 
    public function setDemand($demand)
    {
        $this->demand = $demand;

        return $this;
    }
}