<?php
namespace App\Services\backoffice;

use App\Repository\WebClient\DemandRepository;
use App\Services\backoffice\interface\BackofficeDemandServiceInterface;

/**
 * BackofficeDemandService
 * 
 * @property DemandRepository $demandRepository
 * 
 * @method int getPercentageOfCheckedDemand()
 * @method int getNumberOfUncheckedDemand()
 * 
 * @author Samir Founou <samir_615@live.fr>
 */
class BackofficeDemandService implements BackofficeDemandServiceInterface
{
    private DemandRepository $demandRepository;

    public function __construct(DemandRepository $demandRepository)
    {
        $this->demandRepository = $demandRepository;
    }

    /**
     * Allow to get checked demand percentage
     *
     * @return void
     * @author Samir Founou <samir_615@live.fr>
     */
    public function getPercentageOfCheckedDemand() : int
    {
        $checkedDemands = count($this->demandRepository->getAllCheckedDemands());
        $percentage = 0;

        $checkedDemands != 0 && count($this->demandRepository->findAll()) != 0 ?
        $percentage = round($checkedDemands / count($this->demandRepository->findAll()), 2) * 100 : 0;

        return $percentage;
    }

    /**
     * Allow to get number of unchecked demand
     *
     * @return void
     * @author Samir Founou <samir_615@live.fr>
     */
    public function getNumberOfUncheckedDemand() : int
    {
        return count($this->demandRepository->getUncheckedDemands());
    }
}