<?php
namespace App\Services\backoffice\interface;

interface BackofficeDemandServiceInterface
{
    /**
     * Allow to get checked demand percentage
     *
     * @return void
     * @author Samir Founou <samir_615@live.fr>
     */
    public function getPercentageOfCheckedDemand() : int;

    /**
     * Allow to get number of unchecked demand
     *
     * @return void
     * @author Samir Founou <samir_615@live.fr>
     */
    public function getNumberOfUncheckedDemand() : int;
}