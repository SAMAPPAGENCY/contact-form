<?php
namespace App\Services\Contact;

use App\Entity\WebClient\Demand;
use App\Entity\WebClient\Prospect;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use App\Services\file\CreateJsonFileService;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\WebClient\ProspectRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Services\Contact\interface\DemandServiceInterface;

/**
 * DemandService
 * 
 * @property EntityManagerInterface $manager
 * @property ProspectRepository $manager
 * @property FormInterface $manager
 * @property CreateJsonFileService $manager
 * @property Demand $manager
 * 
 * @method void persistDemand(Request $request, Session $session)
 * @method self setForm(FormInterface $form)
 * @method FormInterface getForm()
 * @method self setDemand(Demand $demand)
 * @method Demand gettDemand()
 * 
 * @author Samir Founou <samir_615@live.fr>
 */
class DemandService implements DemandServiceInterface
{
    private EntityManagerInterface $manager;
    private ProspectRepository $prospectRepository;
    private FormInterface $form;
    private CreateJsonFileService $createJsonFileService;
    private Demand $demand;

    public function __construct(CreateJsonFileService $createJsonFileService, ProspectRepository $prospectRepository, EntityManagerInterface $manager)
    {
        $this->manager               = $manager;
        $this->prospectRepository    = $prospectRepository;
        $this->createJsonFileService = $createJsonFileService;
    }

    /**
     * Allow to persist demand
     *
     * @param Request $request
     * @return void
     * 
     * @author Samir Founou <samir_615@live.fr>
     */
    public function persistDemand(Request $request, Session $session) : void
    {
        // Handle contact form request
        $this->form->handleRequest($request);

        // Test form submission
        if($this->form->isSubmitted() && $this->form->isValid()){
            // Form data object casting
            $data = (object) $this->form->getNormData();

            // Test if prospect is exist in database
            if(!$this->prospectRepository->findOneByMail($data->mail)){
                /**
                 * @var Prospect
                 */
                $prospect = new Prospect;

                $prospect->setFirstname($data->firstname)
                         ->setLastname($data->lastname)
                         ->setMail($data->mail);
            } else {
                /**
                 * @var Prospect
                 */
                $prospect = $this->prospectRepository->findOneByMail($data->mail);
            }

            // Initialization and information affectation of current demand
            $demand = new Demand;
            $demand->setMessage($data->message)
                    ->setProspect($prospect)
                    ->setIsCheck(false)
                    ->setFilename(
                        strtolower($prospect->getLastname()) . '_' . strtolower($prospect->getFirstname()) . '_' . (new \DateTime('now'))->format('d_m_Y') . '_' . uniqid() . '.json'                       
                    );

            // Add current demand to prospect
            $prospect->addDemand($demand);
        
            // Doctrine persistence and flush
            $this->manager->persist($prospect);
            $this->manager->persist($demand);
            $this->manager->flush();

            // Set self demand propetry
            $this->setDemand($demand);

            // Add flash message
            $session->getFlashBag()->add(
                'success',
                "Votre demande a bién été enregistrée, nous vous répondrons dans les meilleurs délais."
            );

            // Write json file into private folder
            $this->createJsonFileService->setDemand($this->getDemand())
                                        ->addJsonFileIntoFolder();
        }
    }

    public function setForm(FormInterface $form) : self
    {
        $this->form = $form;

        return $this;
    }

    public function getForm() : FormInterface
    {
        return $this->form;
    }

    public function setDemand(Demand $demand) : self
    {
        $this->demand = $demand;

        return $this;
    }

    public function getDemand() : Demand
    {
        return $this->demand;
    }
}
