<?php
namespace App\Services\Contact\interface;

use App\Entity\WebClient\Demand;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

interface DemandServiceInterface
{
    /**
     * Allow to persist demand
     *
     * @param Request $request
     * @return void
     * 
     * @author Samir Founou <samir_615@live.fr>
     */
    public function persistDemand(Request $request, Session $session) : void;

    public function setForm(FormInterface $form) : self;

    public function getForm() : FormInterface;

    public function setDemand(Demand $demand) : self;

    public function getDemand() : Demand;
}
