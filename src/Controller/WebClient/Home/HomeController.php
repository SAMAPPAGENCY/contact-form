<?php
namespace App\Controller\WebClient\Home;

use App\Form\Contact\ContactFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\Contact\DemandService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController extends AbstractController
{
    /**
     * Allow to display home page
     * 
     * @Route("/", name="home")
     * 
     * @param Request $request
     * @param SessionInterface $session
     * @param DemandService $demandService
     *
     * @return Response
     * @author Samir FOUNOU <samir_615@live.fr>
     */
    public function index(Request $request, SessionInterface $session, DemandService $demandService): Response
    {
        // Initialization of contact form
        $contactForm = $this->createForm(ContactFormType::class);

        // Initialisation of demand serviec
        $demandService->setForm($contactForm)
                      ->persistDemand($request, $session);

        return $this->render('home/index.html.twig', [
            'contactForm' => $contactForm->createView(),
        ]);
    }
}
