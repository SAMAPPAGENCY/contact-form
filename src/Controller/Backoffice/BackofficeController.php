<?php
namespace App\Controller\Backoffice;

use App\Entity\WebClient\Demand;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\file\DownloadFileService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\WebClient\ProspectRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\backoffice\BackofficeDemandService;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BackofficeController extends AbstractController
{
    /**
     * Allow to show backoffice
     * 
     * @Route("/backoffice", name="backoffice")
     *
     * @param ProspectRepository $prospectRepository
     * 
     * @return Response
     * @author Samir Founou <samir_615@live.fr>
     */
    public function index(BackofficeDemandService $backofficeDemandService, ProspectRepository $prospectRepository): Response
    {
        return $this->render('backoffice/index.html.twig', [
            'prospects'                => $prospectRepository->findAll(),
            'checkedDemandsPercentage' => $backofficeDemandService->getPercentageOfCheckedDemand(),
            'totalOfuncheckedDemands'  => $backofficeDemandService->getNumberOfUncheckedDemand(),
        ]);
    }

    /**
     * Allow to generate indicators info
     * 
     * @Route("/backoffice/generate-indicators-info", name="backoffice_generate_indicators_info")
     *
     * @param BackofficeDemandService $backofficeDemandService
     * @return JsonResponse
     * 
     * @author Samir <samir_615@live.fr>
     */
    public function generateIndicatorsInfo(BackofficeDemandService $backofficeDemandService) : JsonResponse
    {
        return new JsonResponse(
            json_encode(
                [
                    "checkedDemandsPercentage" => $backofficeDemandService->getPercentageOfCheckedDemand(),
                    "totalOfuncheckedDemands"  => $backofficeDemandService->getNumberOfUncheckedDemand(),
                ]
            )
        );
    }

    /**
     * Allow to check demand
     * 
     * @Route("/backoffice/check-demand/{id}", name="backoffice_check_demand")
     *
     * @param Demand $demand
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManagerInterface
     * 
     * @return JsonResponse
     * @author Samir Founou <samir_615@live.fr>
     * 
     */
    public function checkDemand(Demand $demand, SerializerInterface $serializer, EntityManagerInterface $entityManagerInterface) : JsonResponse
    {
        $demand->setIsCheck(true);
        $entityManagerInterface->persist($demand);
        $entityManagerInterface->flush();

        $demandTemplate = new Demand;
        $demandTemplate->setCreatedAt($demand->getCreatedAt())
                       ->setFilename($demand->getFilename())
                       ->setIsCheck($demand->getIsCheck())
                       ->setMessage($demand->getMessage());

        $jsonContent = $serializer->serialize($demandTemplate, 'json');

        return new JsonResponse($jsonContent);
    }

    /**
     * Allow to uncheckDemand
     * 
     * @Route("/backoffice/uncheck-demand/{id}", name="backoffice_uncheck_demand")
     *
     * @param Demand $demand
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManagerInterface
     * 
     * @return JsonResponse
     * @author Samir Founou <samir_615@live.fr>
     */
    public function uncheckDemand(Demand $demand, SerializerInterface $serializer, EntityManagerInterface $entityManagerInterface) : JsonResponse
    {
        $demand->setIsCheck(false);
        $entityManagerInterface->persist($demand);
        $entityManagerInterface->flush();

        $demandTemplate = new Demand;
        $demandTemplate->setCreatedAt($demand->getCreatedAt())
                       ->setFilename($demand->getFilename())
                       ->setIsCheck($demand->getIsCheck())
                       ->setMessage($demand->getMessage());

        $jsonContent = $serializer->serialize($demandTemplate, 'json');

        return new JsonResponse($jsonContent);
    }

    /**
     * Allow to download file
     * 
     * @Route("/backoffice/download-file/{id}", name="backoffice_download_file")
     *
     * @param Demand $demand
     * @param DownloadFileService $downloadFileService
     * 
     * @return JsonResponse
     * @author Samir Founou <samir_615@live.fr>
     */
    public function downloadFile(Demand $demand, DownloadFileService $downloadFileService) : JsonResponse
    {
        try {
            $filecontent = 
                $downloadFileService->readFile(
                    '/private/demand/',
                    $demand->getFilename()
                )
            ;

            return new JsonResponse(
                json_encode(
                    [
                        "type"    => "success",
                        "json"    => json_encode($filecontent),
                        "message" => "Le fichier `" . $demand->getFilename() . "` a été téléchargé avec succès"
                    ]
                )
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                json_encode(
                    [
                        "type"  => "error",
                        "message" => $e->getMessage()
                    ]
                )
            );
        }
    }
}
