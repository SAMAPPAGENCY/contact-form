/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import './styles/login.css';
import './styles/hover.css';

// Jquery
import 'jquery/dist/jquery.js'

// Sb admin template assets
import './styles/sb-admin-2.css'
import './js/sb-admin-2.js'

// Popper
import '@popperjs/core/dist/cjs/popper.js'

// Bootstrap
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

// Datatable
import 'datatables.net/js/jquery.dataTables.js';
import 'datatables.net-bs4/js/dataTables.bootstrap4.js';
import './js/backoffice/datatable.js'

// Notification
import 'notifyjs/dist/notify.js'

// start the Stimulus application
import './bootstrap';
