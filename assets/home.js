

// Jquery
import 'jquery/dist/jquery.js'

// Popper
import '@popperjs/core/dist/cjs/popper.js'

// Bootstrap
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

import './styles/home.css'

// Notification
import 'notifyjs/dist/notify.js'

// start the Stimulus application
import './bootstrap';
